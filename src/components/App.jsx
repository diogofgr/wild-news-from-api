import React, { Component, Fragment } from 'react';
import PostsList from './PostsList';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            posts: []
        };
    }

    onLoadNews = () => {
        // load the news here
    }

    render() {
        const { posts } = this.state;
        return (
            <Fragment>
                <div className="title">
                    <h1>Wild News</h1>
                    <button type="button" className="btn btn-outline-secondary">
                        Load News
                    </button>
                </div>
                <div className="container">
                    <PostsList posts={posts} />
                </div>
            </Fragment>
        );
    }
}

export default App;

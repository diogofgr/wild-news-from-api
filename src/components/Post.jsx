import React from 'react';

const Post = ({ post }) => {
    return (
        <div className="post-card">
            <h3>Here goes the title</h3>
            <h4>Something else here</h4>
            <button type="button" className="btn btn-outline-secondary">Show/Hide</button>
            <p>(the content here should only be visible when the user clicks the post)</p>
        </div>
    );
};

export default Post;
